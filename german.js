/*jslint node:true, vars:true, bitwise:true, unparam:true */
/*jshint unused:true */
/*global */
var mraa = require('mraa'); //require mraa
var myOnboardLed = new mraa.Gpio(13); //LED hooked up to digital pin 13 (or built in pin on Intel Galileo Gen2 as well as Intel Edison)
myOnboardLed.dir(mraa.DIR_OUT); //set the gpio direction to output
var ledState = true; //Boolean to hold the state of Led
var joystick = require('jsupm_joystick12');
var request = require('request');
var express = require('express');
var app = express();
var path = require('path');
var http = require('http').Server(app);
var groveSpeaker = require('jsupm_grovespeaker');
var mySpeaker = new groveSpeaker.GroveSpeaker(2);
var myJoystick = new joystick.Joystick12(0, 1);
var servidor = "http://192.168.43.67:3000";
app.get('/obtenervalor', function(req, res) {
    var r = null;
    var x = roundNum(myJoystick.getXInput()*(-100), 0);
    var y = roundNum(myJoystick.getYInput()*(-100), 0);
    var tiempo = new Date().getTime();
    var pedirDatos = setInterval(function(){
        var x1 = roundNum(myJoystick.getXInput()*(-100), 0);
        var y1 = roundNum(myJoystick.getYInput()*(-100), 0);
        var tiempo_pasado = new Date().getTime();
        if(x1 > x+10){
            r = "abajo";
            clearInterval(pedirDatos);
            console.log(r);
            responder(r, function(){
                return res.send(r);
            });
        }
        if(x1< x-10){
            r = "arriba";
            clearInterval(pedirDatos);
            console.log(r);
            responder(r, function(){
                return res.send(r);
            });
        }
        if(y1 > y+10){
            r = "derecha";
            clearInterval(pedirDatos);
            console.log(r);
            responder(r, function(){
                return res.send(r);
            });
        }
        if(y1< y-10){
            r = "izquierda";
            clearInterval(pedirDatos);
            console.log(r);
            responder(r, function(){
                return res.send(r);
            });
        }
       // console.log(tiempo_pasado+" - "+tiempo+" = "+(tiempo_pasado - tiempo));
         if((tiempo_pasado - tiempo) > 10000){
            r = "nada";
            clearInterval(pedirDatos);
            console.log(r);
            responder(r, function(){
                return res.send(r);
            });
        }
    },100);
});
app.get('/', function(req, res) {
    res.send("holaaaaaaa");
});
app.use(express.static(__dirname + '/client'));
app.use('/client', express.static(__dirname + '/client'));
function roundNum(num, decimalPlaces)
{
	var extraNum = (1 / (Math.pow(10, decimalPlaces) * 1000));
	var numerator = Math.round((num + extraNum) * (Math.pow(10, decimalPlaces)));
	var denominator = Math.pow(10, decimalPlaces);
	return (numerator / denominator);
}
var responder = function(resultado,cb){
    request(servidor+"/respuesta/"+resultado, function (error, response, body) {
          if (!error && response.statusCode == 200) {
              console.log("acertaste");
              console.log(response.body);
              mySpeaker.playSound('g', true, "high");
          }else{
              mySpeaker.playSound('c', true, "low");
              console.log(error);
          }
        });
        cb();
    }
http.listen(3000, function(){
    setTimeout(function() {
        console.log('conectandome al servidor de juegos');
        request.post({url:servidor+"/cargar-usuario", form: {key:'value'}}, function (error, response, body) {
          if (!error && response.statusCode == 200) {
            console.log('me conecte al servidor');
              console.log(response.body);
          }else{
            console.log("error al conectarme");
              console.log(error);
          }
        });
    }, 3000);
});
