var mysql = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'admin',
  password : 'admin123',
  database : 'hackaton'
});

connection.connect();

function getHora(){
  var date = new Date();
  return date.getFullYear() +
  "-"+date.getMonth()+"-"+
  (date.getDate() <= 9 ? "0"+date.getDate():date.getDate()) +
  " "+(date.getHours() <= 9? "0"+date.getHours():date.getHours())+
  ":"+(date.getMinutes() <= 9? "0"+date.getMinutes():date.getMinutes())+
  ":"+(date.getSeconds() <= 9? "0"+date.getSeconds():date.getSeconds())
}

function guardar(usuario){
  var partidaSql = "insert into partidas(fecha) values('"+getHora()+"')";
  connection.query(partidaSql, function(err, result){
    if(err) return console.log(err);
    var partida_id = result.insertId;
    var usuarioSql = "insert into usuarios(nombre, ip, correctas, incorrectas) values(?,?,?,?)";
    //console.log([usuario.nombre, usuario.ip, usuario.correctas, usuario.incorrectas]);
    connection.query(usuarioSql,
      [usuario.nombre, usuario.ip, usuario.correctas, usuario.incorrectas], function(err1, result1){
        if(err1) return console.log(err1);
        var usuario_id = result1.insertId;
        for(var i in usuario.tiempos){
          connection.query("insert into tiempos(usuario, partida, tiempo) values(?, ?, ?)",
          [usuario_id, partida_id, usuario.tiempos[i]], function(uerr, result){
            if(uerr) console.log(uerr);
          });
        }
      });

  });
}

function guardarUsuarios(usuarios){
  for(var i in usuarios){
    guardar(usuarios[i]);
  }
}

function cargarUsuarios(cb){
  var sql = "select partidas.*, tiempos.*, usuarios.* from partidas inner join tiempos on tiempos.partida = partidas.id inner join usuarios on usuarios.id = tiempos.usuario;";
  connection.query(sql, function(err, result){
    if(err) return cb(err);
    cb(result);
  });
}


function usuarios(cb){
  var sql = "select * from usuarios";
  connection.query(sql, function(err, result){
    if(err) return cb(err);
    cb(result);
  });
}

function tiemposUsuario(id,cb){
  var sql = "select * from tiempos where usuario="+id;
  connection.query(sql, function(err, result){
    if(err) return cb(err);
    cb(result);
  });
}

module.exports = {
  guardarUsuarios : guardarUsuarios,
  cargarUsuarios: cargarUsuarios,
  usuarios: usuarios,
  tiempos: tiemposUsuario,
  tablaUsuarios: tablaUsuarios
};
