var express = require('express');
var bodyParser = require('body-parser');
var glob = require('glob');
var request = require("request");
var app = express();
var server = require("http").Server(app);
var io = require('socket.io')(server);
var session = require('express-session');
var base = require("./base.js");
var ultimoNombre = '';
var usuarios = [];
var conexion = {};
var actual = {};
var total = 0;

server.listen(3000);

app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true,
  cookie: { secure: true }
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(express.static('public'));

app.post('/ultimo-nombre', function(req, res){
  if(req.body.nombre == '') return res.status(403).send({
    err_no: 124, summary: 'parametro "nombre" no provisto'
  });

  ultimoNombre = req.body.nombre;

  res.send({
    status: 'ok'
  });
});

app.post('/cargar-usuario', function(req, res){
  if(ultimoNombre == '') return res.status(403).send({
    err_no: 123, summary: 'parametro "nombre" no fue asignado previamente'
  });

  var usuario = {
    ip: req.ip,
    nombre: ultimoNombre
  };

  var ya_esta = false;
  for(var i in usuarios){
    if(usuarios[i].ip == usuario.ip) ya_esta = true;
  }

  if(!ya_esta){
    usuario.correctas = 0;
    usuario.incorrectas = 0;
    usuario.tiempos = [];
    usuarios.push(usuario);
    console.log("Cargar usuarios");
    if(req.session.usuarios == undefined) req.session.usuarios = [];
    req.session.usuarios.push(usuario);
  }

  conexion.notify(usuario);

  ultimoNombre = '';

  res.send(usuario);

});

app.get("/reset", function(req, res){
  ultimoNombre = '';
  usuarios = [];
  actual = {};
  total = 0;
  res.send({
    status: 'ok'
  });
});

app.get("/imagenes", function(req, res){
  //console.log("Gay javi");
  glob(__dirname + "/public/imagenes/**/*.jpg", {}, function(err, files){
    if(err) return res.status(500).send(err);
    var urls = {};
    for(var i in files){
      var partes = files[i].split("imagenes/")[1].split("/");
      if(urls[partes[0]] == undefined){
        urls[partes[0]] = [];
      }
      urls[partes[0]].push(partes[1]);
    }

    var tipos = [];
    for(var i in urls){
      tipos.push(i);
    }

    function getRandom(){
      var i = Math.floor(Math.random() * tipos.length);
      var val = tipos[i];
      tipos.splice(i, 1);
      var num = Math.floor(Math.random() * urls[val].length);
      var nnum = urls[val][num];
      return "imagenes/" + val + "/"+nnum;
    };

    var lugares = ["arriba", "abajo", "izquierda", "derecha"];
    var nuevos = {};
    for(var i = 0; i < 4; i++){
      nuevos[lugares[i]] = getRandom();
    }

    var correcta = lugares[Math.floor(Math.random() * lugares.length)];
    actual = {imagenes: nuevos, correcta: nuevos[correcta]};
    actual.tiempo = (new Date()).getTime();
    res.send(actual);
    for(var i in usuarios){
      var ip = usuarios[i].ip;
      //console.log(ip.replace("::ffff:", "")+":3000/obtenervalor");
      request("http://"+ip.replace("::ffff:", "")+":3000/obtenervalor", function(err, response, body){
        if(err) console.log(err);
        console.log(body);
      });
    }
  });
});

app.get("/respuesta/:dato", function (req, res) {
  var respuesta = req.params.dato;
  var ip = req.ip;
  var usuario = {};
  for(var i in usuarios){
    if(usuarios[i].ip == ip) usuario = usuarios[i];
  }
  var respu = 0;
  if(usuario == {}){
    console.log("No hay usuario");
  }else {
    if(respuesta == "nada"){
      conexion.change(false, ip);
    }else{
    conexion.change(actual.imagenes[respuesta] == actual.correcta? true:false, ip);
  }
    if(actual.imagenes[respuesta] == actual.correcta){
      usuario.correctas++;
      respu = 1;
    }else{
      usuario.incorrectas++;
    }
    total++;
    var t = (new Date()).getTime();
    usuario.tiempos.push(t - actual.tiempo);
    if(total == 4){
      base.guardarUsuarios(usuarios);
      conexion.fin();
    }
    //enviar peticion
  }
  //console.log(usuario);
  if(respu === 0){
    res.status(403).end();
  }else{
    res.status(200).end();
  }
});

app.get("/usuarios", function(req, res){
  //console.log("Consultando: "+req.ip);
  res.send(usuarios);
});

app.get("/info", function(req, res){
  base.cargarUsuarios(function(usuarios){
    return res.send(usuarios);
  });
});

app.get("/tabla-usuarios", function(req, res){
  base.usuarios(function(usuarios){
    return res.send(usuarios);
  });
});

app.get("/tiempos/:id", function(req, res){
  base.tiempos(req.params.id, function(tiempos){
    return res.send(tiempos);
  });
});

app.get("/guardar", function(req, res) {
  var usuarios = [
    {
        "ip": "::ffff:192.168.1.105",
        "nombre": "German",
        "correctas": 1,
        "incorrectas": 3,
        "tiempos": [
            2163,
            6760,
            1527,
            1093
        ]
    }
];
base.guardarUsuarios(usuarios);
res.send({});
});

io.on('connection', function (socket) {
  socket.on("regit", function(){
    console.log("Socket conectado");
    conexion.socket = socket;
    conexion.notify = function(usuario){
      conexion.socket.emit("nuevo-usuario", usuario);
    };
    conexion.change = function(respuesta, ip){
      conexion.socket.emit("respuesta", respuesta, ip);
    };
    conexion.fin = function(){
      conexion.socket.emit("fin");
    }
  });
});
