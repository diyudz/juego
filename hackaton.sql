/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : hackaton

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2015-12-17 20:13:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for partidas
-- ----------------------------
DROP TABLE IF EXISTS `partidas`;
CREATE TABLE `partidas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of partidas
-- ----------------------------
INSERT INTO `partidas` VALUES ('7', '2015-10-08 02:10:18');
INSERT INTO `partidas` VALUES ('8', '2015-10-08 04:02:53');
INSERT INTO `partidas` VALUES ('9', '2015-10-08 04:36:30');
INSERT INTO `partidas` VALUES ('10', '2015-10-08 10:41:49');
INSERT INTO `partidas` VALUES ('11', '2015-10-08 10:45:41');
INSERT INTO `partidas` VALUES ('12', '2015-10-08 11:15:20');
INSERT INTO `partidas` VALUES ('13', '2015-10-08 11:18:49');
INSERT INTO `partidas` VALUES ('14', '2015-10-08 11:20:02');
INSERT INTO `partidas` VALUES ('15', '2015-10-08 11:21:56');
INSERT INTO `partidas` VALUES ('16', '2015-10-08 11:25:38');
INSERT INTO `partidas` VALUES ('17', '2015-10-08 11:27:36');
INSERT INTO `partidas` VALUES ('18', '2015-10-08 11:43:16');
INSERT INTO `partidas` VALUES ('19', '2015-10-08 13:21:09');
INSERT INTO `partidas` VALUES ('20', '2015-11-16 22:33:12');
INSERT INTO `partidas` VALUES ('21', '2015-11-16 22:35:31');
INSERT INTO `partidas` VALUES ('22', '2015-11-17 08:57:05');
INSERT INTO `partidas` VALUES ('23', '2015-11-17 08:59:31');
INSERT INTO `partidas` VALUES ('24', '2015-11-17 09:03:49');
INSERT INTO `partidas` VALUES ('25', '2015-11-17 11:02:29');
INSERT INTO `partidas` VALUES ('26', '2015-11-17 11:51:15');
INSERT INTO `partidas` VALUES ('27', '2015-11-17 11:53:29');
INSERT INTO `partidas` VALUES ('28', '2015-11-17 15:46:09');
INSERT INTO `partidas` VALUES ('29', '2015-11-17 17:35:05');
INSERT INTO `partidas` VALUES ('30', '2015-11-17 17:36:03');
INSERT INTO `partidas` VALUES ('31', '2015-11-17 17:51:13');

-- ----------------------------
-- Table structure for tiempos
-- ----------------------------
DROP TABLE IF EXISTS `tiempos`;
CREATE TABLE `tiempos` (
  `usuario` int(11) NOT NULL,
  `partida` int(11) NOT NULL,
  `tiempo` int(11) NOT NULL,
  KEY `ushack` (`usuario`),
  KEY `parhack` (`partida`),
  CONSTRAINT `parhack` FOREIGN KEY (`partida`) REFERENCES `partidas` (`id`),
  CONSTRAINT `ushack` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tiempos
-- ----------------------------
INSERT INTO `tiempos` VALUES ('3', '7', '6399');
INSERT INTO `tiempos` VALUES ('3', '7', '2872');
INSERT INTO `tiempos` VALUES ('3', '7', '6887');
INSERT INTO `tiempos` VALUES ('3', '7', '9249');
INSERT INTO `tiempos` VALUES ('4', '8', '8258');
INSERT INTO `tiempos` VALUES ('4', '8', '2278');
INSERT INTO `tiempos` VALUES ('4', '8', '4990');
INSERT INTO `tiempos` VALUES ('4', '8', '877');
INSERT INTO `tiempos` VALUES ('5', '9', '5201');
INSERT INTO `tiempos` VALUES ('5', '9', '2047');
INSERT INTO `tiempos` VALUES ('5', '9', '5126');
INSERT INTO `tiempos` VALUES ('5', '9', '1803');
INSERT INTO `tiempos` VALUES ('6', '10', '3163');
INSERT INTO `tiempos` VALUES ('6', '10', '3465');
INSERT INTO `tiempos` VALUES ('6', '10', '12688');
INSERT INTO `tiempos` VALUES ('6', '10', '11456');
INSERT INTO `tiempos` VALUES ('7', '11', '3120');
INSERT INTO `tiempos` VALUES ('7', '11', '1303');
INSERT INTO `tiempos` VALUES ('7', '11', '979');
INSERT INTO `tiempos` VALUES ('7', '11', '1178');
INSERT INTO `tiempos` VALUES ('8', '12', '3368');
INSERT INTO `tiempos` VALUES ('8', '12', '10317');
INSERT INTO `tiempos` VALUES ('8', '12', '10429');
INSERT INTO `tiempos` VALUES ('8', '12', '7303');
INSERT INTO `tiempos` VALUES ('9', '13', '2447');
INSERT INTO `tiempos` VALUES ('9', '13', '1849');
INSERT INTO `tiempos` VALUES ('9', '13', '338');
INSERT INTO `tiempos` VALUES ('9', '13', '1730');
INSERT INTO `tiempos` VALUES ('10', '14', '1520');
INSERT INTO `tiempos` VALUES ('10', '14', '1624');
INSERT INTO `tiempos` VALUES ('10', '14', '2364');
INSERT INTO `tiempos` VALUES ('10', '14', '2098');
INSERT INTO `tiempos` VALUES ('11', '15', '1653');
INSERT INTO `tiempos` VALUES ('11', '15', '856');
INSERT INTO `tiempos` VALUES ('11', '15', '1088');
INSERT INTO `tiempos` VALUES ('11', '15', '992');
INSERT INTO `tiempos` VALUES ('12', '16', '2077');
INSERT INTO `tiempos` VALUES ('12', '16', '1786');
INSERT INTO `tiempos` VALUES ('12', '16', '2491');
INSERT INTO `tiempos` VALUES ('12', '16', '3329');
INSERT INTO `tiempos` VALUES ('13', '17', '1526');
INSERT INTO `tiempos` VALUES ('13', '17', '2159');
INSERT INTO `tiempos` VALUES ('13', '17', '1575');
INSERT INTO `tiempos` VALUES ('13', '17', '1079');
INSERT INTO `tiempos` VALUES ('14', '18', '2235');
INSERT INTO `tiempos` VALUES ('14', '18', '1483');
INSERT INTO `tiempos` VALUES ('14', '18', '2665');
INSERT INTO `tiempos` VALUES ('14', '18', '1277');
INSERT INTO `tiempos` VALUES ('15', '19', '7543');
INSERT INTO `tiempos` VALUES ('15', '19', '10424');
INSERT INTO `tiempos` VALUES ('15', '19', '6292');
INSERT INTO `tiempos` VALUES ('15', '19', '3645');
INSERT INTO `tiempos` VALUES ('16', '20', '2547');
INSERT INTO `tiempos` VALUES ('16', '20', '5254');
INSERT INTO `tiempos` VALUES ('16', '20', '4141');
INSERT INTO `tiempos` VALUES ('16', '20', '3184');
INSERT INTO `tiempos` VALUES ('17', '21', '2581');
INSERT INTO `tiempos` VALUES ('17', '21', '2473');
INSERT INTO `tiempos` VALUES ('17', '21', '3498');
INSERT INTO `tiempos` VALUES ('17', '21', '4104');
INSERT INTO `tiempos` VALUES ('18', '22', '2552');
INSERT INTO `tiempos` VALUES ('18', '22', '2083');
INSERT INTO `tiempos` VALUES ('18', '22', '2691');
INSERT INTO `tiempos` VALUES ('18', '22', '2773');
INSERT INTO `tiempos` VALUES ('19', '23', '10491');
INSERT INTO `tiempos` VALUES ('19', '23', '3420');
INSERT INTO `tiempos` VALUES ('19', '23', '2630');
INSERT INTO `tiempos` VALUES ('19', '23', '2978');
INSERT INTO `tiempos` VALUES ('20', '24', '3340');
INSERT INTO `tiempos` VALUES ('20', '24', '2541');
INSERT INTO `tiempos` VALUES ('20', '24', '1088');
INSERT INTO `tiempos` VALUES ('20', '24', '2480');
INSERT INTO `tiempos` VALUES ('21', '25', '2453');
INSERT INTO `tiempos` VALUES ('21', '25', '5025');
INSERT INTO `tiempos` VALUES ('21', '25', '773');
INSERT INTO `tiempos` VALUES ('21', '25', '555');
INSERT INTO `tiempos` VALUES ('22', '26', '10593');
INSERT INTO `tiempos` VALUES ('22', '26', '3145');
INSERT INTO `tiempos` VALUES ('22', '26', '8863');
INSERT INTO `tiempos` VALUES ('22', '26', '1609');
INSERT INTO `tiempos` VALUES ('23', '27', '5289');
INSERT INTO `tiempos` VALUES ('23', '27', '8094');
INSERT INTO `tiempos` VALUES ('23', '27', '3231');
INSERT INTO `tiempos` VALUES ('23', '27', '10145');
INSERT INTO `tiempos` VALUES ('24', '28', '13721');
INSERT INTO `tiempos` VALUES ('24', '28', '2709');
INSERT INTO `tiempos` VALUES ('24', '28', '2665');
INSERT INTO `tiempos` VALUES ('24', '28', '7214');
INSERT INTO `tiempos` VALUES ('25', '29', '9419');
INSERT INTO `tiempos` VALUES ('25', '29', '5092');
INSERT INTO `tiempos` VALUES ('25', '29', '3860');
INSERT INTO `tiempos` VALUES ('25', '29', '1762');
INSERT INTO `tiempos` VALUES ('26', '30', '7815');
INSERT INTO `tiempos` VALUES ('26', '30', '1886');
INSERT INTO `tiempos` VALUES ('26', '30', '3710');
INSERT INTO `tiempos` VALUES ('26', '30', '346');
INSERT INTO `tiempos` VALUES ('27', '31', '6233');
INSERT INTO `tiempos` VALUES ('27', '31', '6102');
INSERT INTO `tiempos` VALUES ('27', '31', '2938');
INSERT INTO `tiempos` VALUES ('27', '31', '2204');

-- ----------------------------
-- Table structure for usuarios
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `correctas` int(11) NOT NULL,
  `incorrectas` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usuarios
-- ----------------------------
INSERT INTO `usuarios` VALUES ('3', 'Javier', '::1', '1', '3');
INSERT INTO `usuarios` VALUES ('4', 'Miguel', '::1', '1', '3');
INSERT INTO `usuarios` VALUES ('5', 'Pedro', '::1', '2', '2');
INSERT INTO `usuarios` VALUES ('6', 'Iran', '::ffff:192.168.40.170', '2', '2');
INSERT INTO `usuarios` VALUES ('7', 'Raul', '::ffff:192.168.40.170', '3', '1');
INSERT INTO `usuarios` VALUES ('8', 'Tania', '::ffff:192.168.40.170', '1', '3');
INSERT INTO `usuarios` VALUES ('9', 'Liliana', '::ffff:192.168.40.170', '1', '3');
INSERT INTO `usuarios` VALUES ('10', 'Oscar', '::ffff:192.168.40.170', '3', '1');
INSERT INTO `usuarios` VALUES ('11', 'Juan', '::ffff:192.168.40.170', '4', '0');
INSERT INTO `usuarios` VALUES ('12', 'Isaias', '::ffff:192.168.40.170', '3', '1');
INSERT INTO `usuarios` VALUES ('13', 'Marcial', '::ffff:192.168.40.170', '1', '3');
INSERT INTO `usuarios` VALUES ('14', 'Flor', '::ffff:192.168.43.180', '2', '2');
INSERT INTO `usuarios` VALUES ('15', 'Paula', '::ffff:192.168.43.180', '2', '2');
INSERT INTO `usuarios` VALUES ('16', 'German', '::ffff:192.168.43.180', '2', '2');
INSERT INTO `usuarios` VALUES ('17', 'Flor', '::ffff:192.168.43.180', '3', '1');
INSERT INTO `usuarios` VALUES ('18', 'Gloria', '::ffff:192.168.43.180', '4', '0');
INSERT INTO `usuarios` VALUES ('19', 'Pedro', '::ffff:192.168.43.180', '3', '1');
INSERT INTO `usuarios` VALUES ('20', 'carlos', '::ffff:192.168.43.180', '2', '2');
INSERT INTO `usuarios` VALUES ('21', 'german', '::ffff:192.168.43.180', '0', '4');
INSERT INTO `usuarios` VALUES ('22', 'Jugador', '::ffff:192.168.43.180', '2', '2');
INSERT INTO `usuarios` VALUES ('23', 'karla', '::ffff:192.168.43.180', '3', '1');
INSERT INTO `usuarios` VALUES ('24', 'Pedro', '::1', '2', '2');
INSERT INTO `usuarios` VALUES ('25', 'German', '::1', '2', '2');
INSERT INTO `usuarios` VALUES ('26', 'Flor', '::1', '1', '3');
INSERT INTO `usuarios` VALUES ('27', 'Itzel', '::1', '4', '0');
