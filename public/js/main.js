var mainModule = angular.module('mainModule', ['ngRoute', 'jugadoresModule', 'dashboardModule', 'finModule','estadisticasModule']);

mainModule.config(function($routeProvider){

  $routeProvider.when('/app/intro', {
    templateUrl: 'templates/jugadores.html',
    controller: 'jugadoresCtrl'
  })
  .when('/app/dashboard', {
    templateUrl: 'templates/dashboard.html',
    controller: 'dashboardCtrl'
  })
  .when('/app/fin', {
    templateUrl: 'templates/fin.html',
    controller: 'finCtrl'
  })
  .when('/app/estadisticas',{
    templateUrl: 'templates/estadisticas.html',
    controller: 'estadisticasCtrl'
  })
  .otherwise({
    redirectTo: '/app/intro'
  });
});

mainModule.run(function(){
  window.socket = io.connect();
  window.socket.on("connect", function(){
    socket.emit("regit");
  });
});
