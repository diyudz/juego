var estadisticas = angular.module('estadisticasModule',[]);
estadisticas.controller('estadisticasCtrl',function($scope, $http){
	$scope.info = []
	$scope.labels = []
	$scope.valores = []
	$scope.usuarios = []
	console.log("cargando estadisticas");
	$http.get('/tabla-usuarios').success(function(users){
		//debugger;
		$scope.usuarios = users;
		//for(var i in $scope.usuarios){
		angular.forEach($scope.usuarios, function(value, key){
			$http.get('/tiempos/'+$scope.usuarios[key].id).success(function(data){
				//$scope.$apply(function(){
				//console.log(data);
				var t = 0;
				for(var i in data) t += data[i].tiempo;
				t /= data.length;
				//debugger;
					$scope.usuarios[key].tiempo_promedio = t;
				//});
			});
			});
		//}
	}).error(function(error){
		console.log(error);
	});
	$scope.graficar = function(user){
		console.log("graficando");
		$http.get('/tiempos/'+user.id).success(function(data){
			$scope.info = data;

		  for (var i = 0; i < $scope.info.length; i++) {
				$scope.valores[i] = $scope.info[i].tiempo;
				$scope.labels[i] = i+1;
			}
			var data = {
				labels: $scope.labels,
				datasets: [
					{
						label: "Correctas",
						fillColor: "rgba(220,220,220,0.2)",
						strokeColor: "rgba(220,220,220,1)",
						pointColor: "rgba(220,220,220,1)",
						pointStrokeColor: "#fff",
						pointHighlightFill: "#fff",
						pointHighlightStroke: "rgba(220,220,220,1)",
						data: $scope.valores
					}
				]
			};
			var options ={
				scaleShowGridLines : true,
				scaleGridLineColor : "rgba(0,0,0,.05)",
				scaleGridLineWidth : 1,
				scaleShowHorizontalLines: true,
				scaleShowVerticalLines: true,
				bezierCurve : true,
				bezierCurveTension : 0.4,
				pointDot : true,
				pointDotRadius : 4,
				pointDotStrokeWidth : 1,
				pointHitDetectionRadius : 20,
				datasetStroke : true,
				datasetStrokeWidth : 2,
				datasetFill : true,
				legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
			};
			var ctx = document.getElementById("myChart").getContext("2d");
			var myLineChart = new Chart(ctx).Line(data, options);
		}).error(function(data,error){
			console.log("error");
			console.log(data);
			console.log(error);
		});
	}


});
