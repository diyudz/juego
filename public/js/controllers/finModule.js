var finModule = angular.module("finModule", []);

finModule.controller("finCtrl", function($scope, $rootScope, $http){
  $scope.usuarios = [];

  $http.get("/usuarios").success(function(data){
    for(var i in data){
      var total = 0;
      for(var j in data[i].tiempos){
        total += data[i].tiempos[j];
      }
      data[i].total = (total/60)/60;
    }
    $scope.usuarios = data;
  }).error(function(err){
    console.log(err);
  });


  $scope.reset = function(){
    $http.get("/reset").success(function(data){
      $rootScope.jugadores = [];
      location.href="/#/app/intro";
    }).error(function(err){
      console.log(err);
    });
  };

});
