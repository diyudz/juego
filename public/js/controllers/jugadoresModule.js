var jugadoresModule = angular.module("jugadoresModule", []);

jugadoresModule.controller("jugadoresCtrl", function($scope, $http, $rootScope){

  $rootScope.jugadores = [];
  $scope.esperando = false;

  $scope.enviar = function(){
    var nombre = $scope.nombre;
    $http.post("/ultimo-nombre", {
      nombre: nombre
    }).success(function(data){
      $scope.nombre = "";
      $scope.esperando = true;
    }).error(function(err){
      console.log(err);
    });
  };

  $http.get("/usuarios").success(function(data){
    $rootScope.jugadores = data;
  }).error(function(err){
    console.log(err);
  });

  $scope.reset = function(){
    $http.get("/reset").success(function(data){
      $rootScope.jugadores = [];
    }).error(function(err){
      console.log(err);
    });
  };

  

  socket.on("nuevo-usuario", function(usuario){
    $scope.$apply(function(){
      $rootScope.jugadores.push(usuario);
      $scope.esperando = false;
    });
  });



});
