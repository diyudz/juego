var dashboardModule = angular.module("dashboardModule", []);

dashboardModule.controller("dashboardCtrl", function($scope, $http, $rootScope){
  $scope.imagenes = undefined;
  var correcta = "";
  function obtenerImagenes(){
    $http.get("/imagenes").success(function(data){
      $scope.imagenes = data.imagenes;
      var tipos = [];
      for(var i in data) tipos.push(i);
      correcta = data.correcta;
      $scope.correcta = correcta.replace("imagenes/", "").replace(/\/\d\.jpg/, "");
      //mandar peticion
    }).error(function(err){
      console.log(err);
    });
  }

  $http.get("/usuarios").success(function(data){
    $rootScope.jugadores = data;
  }).error(function(err){
    console.log(err);
  });

  $scope.cambiar = function(){
    obtenerImagenes();
  };

  $scope.ok = undefined;

  socket.off("respuesta");
  socket.on("respuesta", function(resp, ip){
    var usuario = {};
    for(var i in $rootScope.jugadores){
      if($rootScope.jugadores[i].ip == ip) usuario = $rootScope.jugadores[i];
    }
    if(usuario == {}){
      console.log("No hay usuario");
    }else {
      console.log(usuario);
      $scope.$apply(function(){
          $scope.ok = {ip: usuario.ip, respuesta: resp};
          setTimeout(function(){
            $scope.$apply(function(){
              $scope.ok = undefined;
              $scope.cambiar();
            });
          }, 2000);
      });
    }
  });

  socket.off("fin");
  socket.on("fin", function(data){
    location.href = "/#/app/fin";
  });

  $scope.cambiar();


});
